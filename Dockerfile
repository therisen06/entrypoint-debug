FROM alpine:latest

RUN apk --no-cache add su-exec

COPY entrypoint /kas/container-entrypoint

ENTRYPOINT ["/kas/container-entrypoint"]
